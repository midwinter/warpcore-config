module.exports = {
  plugins: [
    "@babel/plugin-proposal-class-properties",
    [
      "module-resolver",
      {
        alias: {
          "#events": "./HassEvents"
        }
      }
    ]
  ],
  presets: [
    [
      "@babel/preset-env",
      {
        targets: {
          node: "current"
        }
      }
    ]
  ]
};
