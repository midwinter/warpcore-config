import Warpcore from "warpcorejs";
import HomeAssistantService from "warpcorejs-homeassistant";
import TimeService from "warpcorejs-time";
import dotenv from "dotenv";
import { sendHassAlert } from "./Services/NotificationService";

dotenv.config();

import CraigsLampChanged from "./Events/CraigsLampChanged";
import NightlyAlarm from "./Events/NightlyAlarm";
import LogEvents from "./Events/LogEvents";
import LitterCycleStarted from "./Events/LitterCycleStarted";
import SeedboxSpaceIsLow from "./Events/SeedboxSpaceIsLow";
import ArmHome from "./Tasks/ArmHome";

const {
  HASS_URL,
  HASS_ACCESS_TOKEN,
  BEANSTALK_HOST,
  BEANSTALK_PORT,
  STARTUP_MESSAGE = false
} = process.env;

const warpcore = new Warpcore({
  services: [
    {
      Service: TimeService,
      Events: [NightlyAlarm]
    },
    {
      Service: HomeAssistantService,
      ServiceOptions: {
        hass_url: HASS_URL,
        access_token: HASS_ACCESS_TOKEN
      },
      Events: [SeedboxSpaceIsLow, LogEvents, LitterCycleStarted]
    }
  ],
  tasks: {
    [ArmHome.NAME]: ArmHome
  },
  queue: {
    beanstalkd: {
      host: BEANSTALK_HOST,
      port: BEANSTALK_PORT
    }
  }
});

warpcore.start();
setTimeout(() => {
  STARTUP_MESSAGE &&
    sendHassAlert({ title: "Warpcore", message: "Starting Warpcore" });
}, 1000);
