import { BaseTask, serviceContainer } from "warpcorejs";
import HomeAssistantService from "warpcorejs-homeassistant";

export default class ArmHome extends BaseTask {
  static NAME = "task_arm_home";

  preExecution = async () => {
    const date = new Date();
    const currentHour = date.getHours();
    const hassService = serviceContainer.getService(HomeAssistantService);
    if (!hassService.connected) {
      return BaseTask.Constants.PRE_EXECUTION_RESULTS.REQUEUE;
    }
    if (currentHour > 19 || currentHour < 8) {
      return BaseTask.Constants.PRE_EXECUTION_RESULTS.EXECUTE;
    }
    return BaseTask.Constants.PRE_EXECUTION_RESULTS.DISMISS;
  };

  execute = async () => {
    console.info("Arming Home");
    const hassService = serviceContainer.getService(HomeAssistantService);
    if (!hassService.connected) {
      throw new Error("not connected");
    }

    hassService.callService({
      domain: "alarm_control_panel",
      service: "alarm_arm_home",
      serviceData: {
        entity_id: "alarm_control_panel.149_walnut_street"
      }
    });
  };
}
