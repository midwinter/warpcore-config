import dotenv from "dotenv";
dotenv.config();
import { BaseTask, serviceContainer } from "warpcorejs";
import HomeAssistantService from "warpcorejs-homeassistant";

const { CRAIG_HASS = "notify" } = process.env;

export async function sendHassAlert({
  service = CRAIG_HASS,
  title = "title",
  message = "message"
}) {
  try {
    const hassService = serviceContainer.getService(HomeAssistantService);
    if (!hassService.connected) {
      throw new Error(" not connected");
    }
    hassService.callService({
      domain: "notify",
      service,
      serviceData: { title, message }
    });
  } catch (e) {
    console.error(e);
  }
}
