import { BaseEvent, serviceContainer } from "warpcorejs";
import HomeAssistantService from "warpcorejs-homeassistant";
import moment from "moment";
import ArmHome from "../../Tasks/ArmHome";

export default class NightlyAlarm extends BaseEvent {
  meetsCondition = () => {
    const time = moment().format("HH:mm:ss");
    if (time !== "22:30:00") {
      return false;
    }
    const hassService = serviceContainer.getService(HomeAssistantService);
    return hassService
      .getState("alarm_control_panel.149_walnut_street")
      .then(entity => {
        if (entity.state === "disarmed") {
          return true;
        }

        return false;
      });
  };

  action = data => {
    console.info("dispatching task arm alarm");

    this.dispatchTask({
      taskName: ArmHome.NAME,
      taskData: data
    });
  };
}
