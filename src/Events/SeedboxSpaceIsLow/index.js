import { BaseEvent } from "warpcorejs";
import { utils as hass_utils } from "warpcorejs-homeassistant";
import { sendHassAlert } from "../../Services/NotificationService";
const THRESHOLDS = [750, 500, 250, 100];
export default class SeedboxSpaceIsLow extends BaseEvent {
  meetsCondition = hassData => {
    if (!hass_utils.isStateChangedEvent({ hassData })) {
      return false;
    }
    const { entity_id } = hassData.data;
    if (entity_id != "sensor.seedbox_free_space") {
      return false;
    }
    return this.getCrossedThresholds(hassData).length > 0;
  };

  action = hassData => {
    const { new_val } = this.getSpaceStates(hassData);
    sendHassAlert({
      title: "Seedbox",
      message: `Seedbox is low on space. ${new_val} gigs remaining.`
    });
  };

  getSpaceStates(hassData) {
    const { new_state, old_state } = hassData.data;
    const old_val = parseFloat(old_state.state);
    const new_val = parseFloat(new_state.state);
    return { old_val, new_val };
  }
  getCrossedThresholds(hassData) {
    return THRESHOLDS.filter(threshold => {
      const { old_val, new_val } = this.getSpaceStates(hassData);
      return old_val >= threshold && new_val < threshold;
    });
  }
}
