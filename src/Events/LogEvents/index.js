import { BaseEvent, serviceContainer } from "warpcorejs";
import HomeAssistantService from "warpcorejs-homeassistant";

export default class LogEvents extends BaseEvent {
  meetsCondition = () => {
    return true;
  };

  action = data => {
    console.log(data);
  };
}
