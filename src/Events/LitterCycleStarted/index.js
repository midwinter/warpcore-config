import { BaseEvent } from "warpcorejs";
import { utils as hass_utils } from "warpcorejs-homeassistant";
import { sendHassAlert } from "../../Services/NotificationService";

export default class LitterCycleStarted extends BaseEvent {
  meetsCondition = hassData => {
    if (!hass_utils.isStateChangedEvent({ hassData })) {
      return false;
    }
    const { entity_id, new_state } = hassData.data;
    return (
      entity_id === "sensor.litter_robot_borg_sphere_status" &&
      new_state.state === "Clean Cycling"
    );
  };

  action = data => {
    sendHassAlert({
      title: "Litter Robot",
      message: "Cleaning Cycle Started"
    });
  };
}
