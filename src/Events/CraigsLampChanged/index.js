import { BaseEvent } from "warpcorejs";
import { utils as hass_utils } from "warpcorejs-homeassistant";
import ArmHome from "../../Tasks/ArmHome"

export default class CraigsLampChanged extends BaseEvent {
  meetsCondition = hassData => {
    return hass_utils.entityWentOff({ hassData, entityId: "light.entryway" });
  };

  action = data => {
    console.info('dispatching task arm alarm');

    // this.dispatchTask({
    //   taskName: ArmHome.NAME,
    //   taskData: data,
    // });
  };
}
