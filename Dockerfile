FROM node:10-alpine as base
WORKDIR /app

FROM base AS development
COPY package.json ./
RUN yarn install --production
RUN cp -R node_modules /tmp/node_modules
COPY . .
RUN yarn install

FROM development as builder
RUN yarn babel ./src --out-dir ./dist --copy-files

FROM base AS release
RUN apk add --no-cache tzdata
ENV TZ America/Winnipeg
RUN yarn global add nodemon
COPY --from=builder /tmp/node_modules ./node_modules
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/package.json ./
CMD ["nodemon", "dist/index.js"]
